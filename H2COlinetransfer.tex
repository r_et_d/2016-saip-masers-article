\documentclass[a4paper]{jpconf}
\usepackage{graphicx,url}

\bibliographystyle{iopart-num}

\begin{document}

%\title{Modelling radiative line transfer in maser environments}
\title{Solving the radiative transfer equation for maser environments}
\author{R van Rooyen$^1$ and DJ van der Walt$^2$}
\address{$^1$ SKA South Africa, 3rd Floor, The Park, Park Road, Pinelands, 7405, Western Cape, South Africa}
\address{$^2$ Department of Space Physics, North-West University, Potchefstroom Campus, 11 Hoffman Street, Potchefstroom, 2531}

\ead{ruby@ska.ac.za}

\begin{abstract}
%An astrophysical maser is a naturally occurring source of stimulated spectral line emission. To generate maser radiation, a molecular population inversion is required. This non-equilibrium inversion can be created by various pumping mechanisms, most typically infrared radiation and collisions. Spectral line emission from masers is stimulated and monochromatic, meaning the emitted photons have the same frequency and direction as the original photon, causing great amplification of maser emission. In order to understand the pumping mechanisms and physical characteristics of masers, suitable models for the molecular excitation and radiative transfer must be developed. The ``masers'' package is developed in Python and solves line transfer equations assuming the escape probability approximation method and using molecular data and parameters describing the physical environment. It extends existing radiative transfer software by providing a reasonably fast, stable algorithm that deals with the solution method's inherent sensitivity to oscillations and multiple valid outcomes; allows different maser geometries for calculation; includes the contribution of interacting background radiation fields, as well as other sources of opacity such as line overlap.
The study of astrophysical maser formation provides a method for probing the chemical composition of the sources they are observed in. In order to understand the pumping mechanisms and physical characteristics of masers, suitable models must contain expressions for each level population from which an inverse function that will cause the mean line intensity to move away from black body form can be derived. Central to this is obtaining numerically consistent solutions to the population density distributions of the maser molecules.  The ``masers'' package is developed in Python and implements the escape probability approximation method. It solves the level population problem for non-LTE statistical equilibrium using molecular data and parameters describing the physical environment. It extends existing radiative transfer software by providing a reasonably fast, stable algorithm that deals with the solution method's inherent sensitivity to oscillations and multiple valid outcomes; allows different maser geometries for calculation; includes the contribution of interacting background radiation fields, as well as other sources of opacity such as line overlap.
\end{abstract}

\section{Introduction}

%The equations of radiative transfer and level population are a set of coupled equations that have to be solved simultaneously.

%The escape probability method decouples the statistical population rate equations from the radiative transfer problem by introducing a local multiplication factor, $\beta$, describing the probability that a photon will escape the system after it is generated~\cite{elitzur1992}. This escape probability factor only depends on the local geometry and optical depth.

%By decoupling the equations the solution can be expressed as a matrix of coefficients acting on a vector of populations. In this form, the level population densities can be obtained by a number of standard numerical methods, with the only prerequisite for a successful solution being a reasonable initial guess.

Determining the level population distribution in an arbitrary situation is the central problem in modelling spectral line emission in general and maser radiation in particular. In essence this consists of a two part solution: Solve the rate-equation problem for the populations of the energy levels under conditions that will produce an inversion. Then, calculate the amplification of the maser radiation through the medium sustaining the inversion.

The escape probability approximation method separates population density calculations from the line radiation in the radiative transfer, RT, equation~\cite{elitzur1992}. This is one of the most implemented methods for solving the RT equation and software implementing this method for the study of thermal line emission are readily available. Care should be taken however, since obtaining numeric solutions under conditions causing inversion in maser environments are a little more tricky. In this paper we present easily overlooked numerical failures found during a study investigating physical conditions to investigate population inversion leading to formaldehyde masers.


\section{Theory and Calculation}
Applying the escape probability to the rate equations, the level populations calculation can be expressed as presented in Equation 2.7.1 from~\cite{elitzur1992}.
\begin{equation}
\label{eq:escapeproprate}
\begin{array}{rcl}
\frac{dn_i}{dt} & = &
- \sum\limits_{j < i} \left\{A_{ij}\beta_{ij}\left[n_i + W\aleph_{ij}(n_i - n_j)\right]  +
C_{ij}\left[n_i - n_j\exp\left(\frac{-h\nu_{ij}}{kT}\right)\right]\right\} \\
& + &
\sum\limits_{j > i} \frac{g_j}{g_i}\left\{A_{ji}\beta_{ji}\left[n_j + W\aleph_{ji}(n_j - n_i)\right]  +
C_{ji}\left[n_j - n_i\exp\left(\frac{-h\nu_{ji}}{kT}\right)\right]\right\} \\

%\frac{dn_i}{dt} & = &
%- \sum\limits_{j < i} \left\{A_{ij}\beta_{ij}\left[n_i + X_{ij}(n_i - n_j)\right]  +
%C_{ij}\left[n_i - n_j\exp\left(\frac{-h\nu_{ij}}{kT}\right)\right]\right\} \\
%& + &
%\sum\limits_{j > i} \frac{g_j}{g_i}\left\{A_{ji}\beta_{ji}\left[n_j + X_{ji}(n_j - n_i)\right]  +
%C_{ji}\left[n_j - n_i\exp\left(\frac{-h\nu_{ji}}{kT}\right)\right]\right\} \\
\end{array}
\end{equation}
where $N_i = g_in_i$, $g_i$ is the statistical weight of level $i$, $N_i$ represents the number density in level $i$, $W$ is the dilution factor and $\aleph_{ij}$ the photon occupancy number at transition frequency $\nu_{ij}$.
%where $N_i$ represents the number density in level $i$, $g_i$ is the statistical weight of level $i$ and $N_i = g_in_i$. The background radiative contribution can be expressed as $X = \beta_{ij}W_{HII}\aleph_{HII} + \beta_{ij}W_d\aleph_d + \aleph_{BB}$ with photon occupancy number $\aleph_{ij}$ and $\beta=\frac{1-\exp(-\tau)}{\tau}$.

To obtain the matrix equation, rewrite the parameters of Equation~\ref{eq:escapeproprate}, grouping all populating and depopulating levels and substitute the radiative and collisional components
\[
\begin{array}{rcl}
R_{ij} & = & \left\{
\begin{array}{ll}
A_{ji}\beta_{ji}(1 + X_{ji}) & i<j \\
A_{ij}\beta_{ij}\left(\frac{g_i}{g_j}\right)X_{ij} & j>i \\
\end{array} \right . \\
R_{ii} & = & - \sum\limits_{i \neq j}R_{ji} \\
\widetilde{C}_{ij} & = & C_{ji} \\
\widetilde{C}_{ii} & = & - \sum\limits_{i \neq j}C_{ij} \\
\end{array}
\]
Thus obtaining the form:
$$\bf{Q}\bf{x} = \bf{b}$$ where ${\bf{b}} = [0,\cdots, 0, 1]^T$, $x_i = \frac{n_i}{n_{mol}}$ the normalised fractional population density with $n_{mol}$ the total population density of the molecule and $\mathbf{Q} = \mathbf{R} + \mathbf{\widetilde{C}}$.

The equation can be solved using iterative methods, such as LU-factorization or the singular value decomposition (SVD). The required initial solution is obtained by calculating the level population at local thermal equilibrium (LTE).

\section{Masers Python Package}
\subsection{Implementation}
The {\tt masers} package solves for statistical equilibrium given some parameters to describe the physical environment. The most basic parameters that must be provided are the density of the cloud, the abundance of the maser molecule, various temperatures, and the column density. To calculate the optical depth, the anticipated size and geometry of the cloud must also be provided, as well as any and all diffuse background radiation components that may interact with the line emission.

Since the solution of the radiative transfer equation is dependent on the molecular level populations, molecular rate and collision coefficients must also be provided. These coefficients are obtained from the molecular data files in the Leiden LAMBDA database~\cite{lambda2005}, \url{http://home.strw.leidenuniv.nl/~moldata}.

During development, we used the RADEX non-LTE excitation and radiative transfer code~\cite{radex2004}, available from the Leiden website, for comparison and verification; note that both methods solve the rate equations iteratively.  Iterative methods are sensitive to oscillation and multiple valid solutions since the solution of the previous iteration is used as the initial estimate for the next iteration and the process is repeated until the input estimate and calculated solution have reached some convergence criteria.

Numerical stability is evaluated by calculating the optical depth and excitation temperature for formaldehyde molecular data~\cite{H2COLeiden}, gas kinetic temperature $T_k=300$\,K, $H_2CO$ fractional abundance $X_{H_2CO}=5\times10^{-7}$, specific column density $\frac{N_{col}}{\Delta v}=10^{6}, 10^{9}, 10^{12}$\,cm$^{-3}$s respectively and $T_{BB}=30$\,K as blackbody radiator, private communication Prof van der Walt. The equilibrium solution for total density ranging from $10^3$--$10^{12}$\,cm$^{-3}$ is calculated using a convergence limit of $|x_n-x_{n-1}|/x_{n-1} < 10^{-3}$, with $x_n$ and $x_{n-1}$ is the level populations calculated during the current and previous evaluation.

Possible convergence criteria includes using population densities or excitation temperature calculations, via the relation $\frac{n_u}{n_l}=\frac{g_u}{g_l}\exp(\frac{-h\nu}{kT_{ex}})$. The effect of choosing either convergence criteria for the parameters above was inspected for specific column density $\frac{N_{col}}{\Delta v}=10^{12}$\,cm$^{-3}$s. The {\tt masers} implementation uses the population densities calculated at each iteration. {\tt RADEX} evaluates the average difference between the excitation temperature calculated from the previous solution compared to the calculation of the current solution (from source code). Inspection of excitation temperature calculation as convergence criteria showed sensitivity to oscillating solutions, Figure~\ref{fig:oscillate}, caused by catastrophic cancellation due to the difference of small numbers in the denominator of the excitation temperature calculation, $T_{ex}\propto[\ln(x_lg_u)-\ln(x_ug_l)]^{-1}$. {\tt RADEX} minimises this by only selecting certain levels to evaluate convergence (from source code).

%This behaviour can be shown using formaldehyde molecular data~\cite{H2COLeiden} and setting the gas kinetic temperature $T_k=300$\,K, $H_2CO$ fractional abundance $X_{H_2CO}=5\times10^{-7}$, specific column density $\frac{N_{col}}{\Delta v}=10^{6}, 10^{9}, 10^{12}$\,cm$^{-3}$s respectively and $T_{BB}=30$\,K as blackbody radiator. The equilibrium solution for total density ranging from $10^3$--$10^{12}$\,cm$^{-3}$ is calculated using a convergence limit of $|x_n-x_{n-1}|/x_{n-1} < 10^{-3}$, with $x_n$ and $x_{n-1}$ is the level populations calculated during the current and previous evaluation.

%The effect of the difference convergence criteria between the two methods for the parameters above was inspected for the case where specific column density $\frac{N_{col}}{\Delta v}=10^{12}$\,cm$^{-3}$s. This was done by calculating the population densities, excitation temperatures and optical depth for all iterations over all total densities. A total of 80 iterative solutions were captured over the total density range and the calculated values for transition $4_{14}$--$3_{13}$ shown in Figure~\ref{fig:oscillate}.

Excitation temperature values computed at all iterations over total density range are shown in the top graph of Figure~\ref{fig:oscillate}. The centre and bottom graphs shows the effect carried forward and amplified in the optical depth and level population calculations. The red line in the bottom graph shows the population densities for the lower level of the transition as they were calculated over iterations, while the blue line shows the population densities for the upper transition level.  Slight step functions show the total density steps, but the population densities clearly show oscillating behaviour around $n_{H_2}=10^6$--$10^8$\,cm$^{-3}$.

\begin{figure}[h]
\centering
\begin{minipage}{.49\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/oscillating_solutions_norelaxation}
%  \caption{\label{fig:oscillate}\small Iterative solution to equilibrium for $H_2CO$ with parameters $T_k=300$\,K, $X_{H_2CO}=5\times10^{-7}$, $\frac{N_{col}}{\Delta v}=10^{12}$\,cm$^{-2}$ and $T_{BB}=30$\,K. Excitation temperature, optical depth and population density solution per iteration over total density range $n_{H_2}=10^6-10^8$\,cm$^{-3}$.}
  \caption{\label{fig:oscillate}\small Oscillating behaviour seen in convergence evaluation using excitation temperature evaluation. The convergence results are seen in the $4_{14}$--$3_{13}$ transition showing 80 iterations over total density range $n_{H_2}=10^6-10^8$\,cm$^{-3}$ and specific column density $\frac{N_{col}}{\Delta v}=10^{12}$\,cm$^{-3}$s.}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/stable_solution_relaxation}
  \caption{\label{fig:solution}\small A ratio of 9.5:0.5 means that only 5\% is given to the new calculation, pulling the iterative solution
close to the previous behaviour. This ``memory'' stabilizes the iterative calculation by ensuring it is not independent, but is influenced by the previous results.}
\end{minipage}
\end{figure}

Oscillating behaviour is only pronounced at lower transition levels and the solutions were found to become more stable if the next iteration is given some ``memory'' of the previous solutions. This was done using a running average calculation, $x_n = c_1\times x_n + c_2\times x_{n-1}$ where $c_1\leq1$ and $c_2\leq1$ are some weighting coefficients with $c_1+c_2=1$. The larger the coefficient for the previous solution $x_{n-1}$ the ``longer'' the memory of the solution is. For {\tt masers} this ``memory'' was found to be fairly large with $x_n = 0.05\times x_n + 0.95\times x_{n-1}$ providing solutions under all simulated conditions. It should be noted that a ``long memory'', $c_2 \rightarrow 1$, requires a more stringent convergence limit. For {\tt masers} this is found to be $|x_n-x_{n-1}|/x_{n-1} < 10^{-7}$ to obtain solutions shown in Figure~\ref{fig:solution}.

Lastly, the calculation of optical depth between the 2 implementations is also found to be significant. The {\tt masers} solver applies the large velocity gradient, LVG, definition for the escape probability calculation~\cite{elitzur1992}. When results are compared to solutions obtained from {\tt RADEX} care must be taken to ensure no large negative optical depth values. For thermal radiative transfer calculations the optical depth is always expected to be positive, thus {\tt RADEX} substitutes the turbulent medium escape probability calculation for $|\tau| \geq 7$ (from source code). This escape probability calculation requires the computation of $\sqrt{\tau}$, which fails for maser regions when inversion causes the optical depth to be negative~\cite{radex2004}.

Stabilising {\tt masers} against the highlighted numerical sensitivities provides a robust solver to use during the inspection of maser pumping parameter space. Figure~\ref{fig:compare} shows the improvement at selected levels known to cause numerical failures in {\tt RADEX}. The top row shows results from the {\tt masers} solver, while the bottom row shows results from the {\tt RADEX} code. The four columns show {\tt Tex, log10(Tex), Tau, log10(Tau)} for selected transition levels $1_{10}$--$1_{11}$ and $12_{1\,11}$--$11_{1\,10}$.
\begin{figure}[h]
\centering
\begin{minipage}{.9\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/110-111}
\end{minipage}
\hfill
%\begin{minipage}{.9\textwidth}
%  \centering
%  \includegraphics[width=.95\linewidth]{images/414-313}
%\end{minipage}
%\hfill
\begin{minipage}{.9\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/12111-11110}
\end{minipage}
\caption{\small The first and third rows show results from the {\tt masers} solver with colour scales showing specific molecular column density $\frac{N_{col}}{\Delta v}=10^{6}$ (green), $10^{9}$ (red), $10^{12}$ (blue) for all $H_2CO$ transitions. The second and last rows show results obtained from the {\tt RADEX} solver with specific molecular column density $\frac{N_{col}}{\Delta v}=10^{6}$ (blue), $10^{9}$ (green), $10^{12}$ (red) for all $H_2CO$ transitions.}
\label{fig:compare}
\end{figure}


\subsection{Verification}
Following methodology suggested by Prof van der Walt, in a private communication, the basic functionality of the {\tt masers} software could be verified. This methodology describes the inspection of the pumping and inversion of $H_2CO$ masers in star-forming region G37.55+0.20, specifically looking at the optical depth calculation for the 4.8\,GHz maser associated with the $1_{10}$--$1_{11}$ transition as a function of the specific column density.

Assuming fractional molecular density $X_{H_2CO}= 5\times 10^{-7}$ and electron temperature $T_e=10^4$\,K, Figure~\ref{fig:pumping} shows example calculations of the following three physical environments:
\begin{itemize}
\item $n_{H_2} = 4.4\times 10^5$\,cm$^{-3}$, $T_d=100$\,K, $T_{k}=300$\,K, $w_d=0.1$ and no $H_{II}$\/ region,
\item $n_{H_2} = 10^4$\,cm$^{-3}$, $T_{k}=20$\,K, $w_{H_{II}}=0.1$, $EM=6\times 10^9$\,cm$^{-6}$\,pc and no dust,
\item $n_{H_2} = 10^4$\,cm$^{-3}$, $T_d=100$\,K, $w_d=0.1$, $T_{k}=300$\,K, $w_{H_{II}}=0.1$,
$EM=6\times 10^9$\,cm$^{-6}$\,pc.
\end{itemize}

\begin{figure}[h]
\centering
\begin{minipage}{.49\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/pumpinginversion}
  \caption{\label{fig:pumping}\small Three examples showing variation of optical depth calculation of the $1_{10}$--$1_{11}$ transition. Physical parameters were kept constant over the range of specific column density values, showing the maximum optical depth identified.}
\end{minipage}
\hfill
\begin{minipage}{.49\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/dustinversion}
  \caption{\label{fig:dust}\small These results indicates that the inversion is unrelated to the dust temperature, which suggests that radiative pumping by the dust infrared radiation field is not significant for the maser.}
\end{minipage}
\end{figure}

Similarly, Figure~\ref{fig:dust} shows results for optical depth calculation with dust temperatures using $T_d=50,\,100,\,200$\,K, given $n_{H_2}=4.4\times 10^5$\,cm$^{-3}$, $T_{k}=300$\,K, $w_d=0.1$ and no $H_{II}$\/ region.

Numerical results obtained were compared to results obtained by Prof van der Walt using independent numerical code solving the rate equations using Heun's method.


\subsection{Pumping}
Applying the {\tt masers} software to investigate the pumping of the $H_2CO$ maser in G37.55+0.20 as described in~\cite{vdWalt2014} showed good comparison between results. Both methods showed that collisional excitation with $H_2$ as well as radiative excitation by the free-free radio continuum radiation from a nearby ultra- or hyper-compact $H_{II}$ region can invert the $1_{10}$--$1_{11}$ (4.8\,GHz) transition.

\begin{description}
\item[Collisional excitation] Figure~\ref{fig:colpumping} shows the optical depth of $1_{10}$--$1_{11}$ transition as a function of $H_2CO$ specific column density over range $10^8$--$10^{14}$\,cm$^{-3}$\,s and $T_{k} = 140, 180, 220, 260, 300$\,K,  $n_{H_2} = 4\times 10^5, 2.6\times 10^5, 1.6\times 10^5, 1.3\times 10^5, 7.5\times 10^4$\,cm$^{-3}$. Visual comparison shows results are duplicate of those shown in Fig 1 presented in~\cite{vdWalt2014}.
\item[Radiative excitation via the dust continuum emission] The dependence of optical depth on the specific column density is shown in Figure~\ref{fig:dustpumping}, for $T_{k} = 180$\,K, $n_{H_2} = 1.3\times 10^5$\,cm$^{-3}$, $w_d = 1.0$ and $T_d = 0$\,K (no dust), $100, 180$\,K. Comparison shows results are duplicate of those shown in Fig 3 presented in~\cite{vdWalt2014}.
\item[Radiative excitation through the free-free continuum emission] from an ultra- or hyper-compact $H_{II}$\/ region is shown in Figure~\ref{fig:freefree} using emission measure of the $H_{II}$\/ region $EM=10^{10}$\,pc\,cm$^{-6}$, as well as $T_e=10^4$\,K, $w_{H_{II}}=1.0$, $T_{k} = 140, 180, 220, 260, 300$\,K, and $n_{H_2} = 4\times 10^5, 2.6\times 10^5, 1.6\times 10^5, 1.3\times 10^5, 7.5\times 10^4$\,cm$^{-3}$. Comparison shows results are duplicate of those shown in Fig 6 presented in~\cite{vdWalt2014}.
\end{description}
\begin{figure}[h]
\centering
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/colexcite}
  \caption{\label{fig:colpumping}\small Effect of collisions on the level populations in the absence of radiation fields.}
\end{minipage}
\hfill
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/dustexcite}
  \caption{\label{fig:dustpumping}\small Optical depth as a function of specific column density.}
\end{minipage}
\hfill
\begin{minipage}{.32\textwidth}
  \centering
  \includegraphics[width=\linewidth]{images/freefreeexcite}
  \caption{\label{fig:freefree}\small Free-free emission has a significant effect on the inversion.}
\end{minipage}
\end{figure}

%\section{Line Overlap}
%If another source of opacity, such as an overlapping line or continuum is present, a photon can be lost to the line even though it does not escape the source.

%Extending the matrix equation to make provision for line overlap contributions requires and update to the radiative matrix contribution to $\mathbf{R} = \mathbf{R^a} + \mathbf{R^b} - \mathbf{R^{ab}}$.

%\[
%\begin{array}{rcl}
%R^a_{ij} & = & \left\{
%\begin{array}{ll}
%A_{ji}\beta^a_{ji}(1 + X_{ji}) & i<j \\
%A_{ij}\beta^a_{ij}\left(\frac{g_i}{g_j}\right)X_{ij} & j>i \\
%- \sum\limits_{i \neq j}R_{ji} & j=i \\
%\end{array} \right .
%\end{array}
%\begin{array}{rcl}
%R^{ab}_{ij} & = & \left\{
%\begin{array}{ll}
%A_{ji}\beta^{ab}_{ji}(x_a + X_{ji}) & i<j \\
%A_{ij}\beta^{ab}_{ij}\left(\frac{g_i}{g_j}\right)X_{ij} & j>i \\
%- \sum\limits_{i \neq j}R_{ji} & j=i \\
%\end{array} \right .
%\end{array}
%\begin{array}{rcl}
%R^b_{ij} & = & \left\{
%\begin{array}{ll}
%A_{ji}\beta^b_{ji} & i<j \\
%A_{ij}\beta^b_{ij} & j>i \\
%0 & j=i \\
%\end{array} \right .
%\end{array}\, ,
%\]
%where $\beta^a = [1-(1-f_a)(1-\beta_{\tau_a})]$, $\beta^{ab}=f_a(1-\beta_{\tau_a+\tau_b})$, $\beta^b=f_bx_a(1-\beta_{\tau_a+\tau_b})$ and $x_a=\frac{\kappa_a}{\kappa_{ab}}$ and $f_a = \frac{\Delta_{ab}}{\Delta_a}$ the absorption and overlap ratios respectively~\cite{elitzur1985}.

\section{Summary}
%Standard matrix inversion methods used in the calculation of the line transfer equation need to be more robust to ensure good solutions in the simulated maser environments used to model various pumping schemes. The {\tt masers} implementation provides a simple to use Python package that shows good agreement with published results.

The paper highlights various reasons why standard matrix inversion methods used in the calculation of thermal line transfer models need to be more robust to ensure good results in the stimulated maser environments used to model maser pumping schemes.

Verification could be achieved through personal communication with Prof van der Walt who has independent software for solving the rate equations using Heun's method.

Validation of the implementation is followed by comparison between the results obtained with the {\tt masers} package to results published in~\cite{vdWalt2014}. Visual comparison of graphs showing results of example parameters inspecting the pumping of the $H_2CO$ maser in G37.55+0.20 shows good agreement. 

\section*{References}
\bibliography{H2COlinetransfer}
%\begin{thebibliography}{9}

%\bibitem{elitzur1992} Elitzur M 1992 {\tt Astronomical Masers} (Netherlands: Kluwer Academic Publishers)

%\bibitem{lambda2005} Sch{\"o}ier F\,~L, van der Tak F\,~F\,~S, van Dishoeck E\,~F and Black J\,~H An atomic and molecular database for analysis of submillimetre line observations 2005 {\tt A\&A } {\bf 432} 369-79

%\bibitem{radex2004} Van der Tak F\,~F\,~S, Black J\,~H, Sch{\"o}ier F\,~L, Jansen D\,~J and van Dishoeck E\,~F A computer program for fast non-LTE analysis of interstellar line spectra. With diagnostic plots to interpret observed line intensity ratios 2007 {\tt A\&A} {\bf 468} 627-35

%\bibitem{H2COLeiden} Wiesenfeld L and Faure A Rotational quenching of $H_2CO$ by molecular hydrogen: cross-sections, rates and pressure broadening 2013 {\tt MNRAS} {\bf 432} 2573-78

%\bibitem{vdWalt2014} Van der Walt D\,~J {\tt Pumping of the 4.8 GHz H$_{2}$CO masers and its implications for the periodic masers in G37.55+0.20} 2014 {\tt A\&A} {\bf 562} A68

%\bibitem{elitzur1985} Elitzur M and Netzer H {\tt Line fluorescence in astrophysics} 1885 {\tt ApJ} {\bf 291} 464-67
%\end{thebibliography}

\end{document}